### FaaS介绍

FaaS，Function as a Service，函数即服务，是一种Serverless无服务器架构模式（不是不需要服务器，而是无需关注服务器）。FaaS使得使用者只需要关注将业务要求通过函数的方式表达，托管平台会自动在事件到来时触发运行，使用者无需关注运行其函数的服务器的具体细节。这些业务要求通常是一些短时的工作，如数据处理，文件管理等等。FaaS最后部署的基本单元就是一个个具备触发条件的函数。FaaS的函数环境在函数运行完就会动态的销毁。

![FaaS1](../images/faas1.jpg)

FaaS使用的场景：

- 事件驱动型程序

  某些事件发生时，对应的处理函数
- 批处理型程序
- 轻量级任务

  无需使用服务器，浪费太多资源，简单的静态网站等等
- 无服务器程序
- 实时数据流处理

![FaaS实例](../images/faas%E5%AE%9E%E4%BE%8B.png)



### 参考链接

[10分钟！FAAS！_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1tg4y1u7DR/?vd_source=ab32fbac61c958a8816cc8ed84cb6438)

[Debian9安装docker - 杰尔克 - 博客园 (cnblogs.com)](https://www.cnblogs.com/jokerxtr/p/15387425.html)

[动手搭建ServerLess服务 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/413246974)

[GitHub - fnproject/fn: The container native, cloud agnostic serverless platform.](https://github.com/fnproject/fn#top)

[Fn Project - The Container Native Serverless Framework](https://fnproject.io/)

[Introduction to Fn with Python (fnproject.io)](https://fnproject.io/tutorials/python/intro/)
